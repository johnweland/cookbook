
var $okRow;
var $lastChecked;
var $checkbox;

function toggleDrawer() {
    $('.drawer__panel').toggleClass('closed');
    if ($('.drawer__toggle > span.fa').hasClass('fa-angle-left')) {
        $('.drawer__toggle > span.fa').removeClass('fa-angle-left');
        $('.drawer__toggle > span.fa').addClass('fa-angle-right');
    } else {
        $('.drawer__toggle > span.fa').removeClass('fa-angle-right');
        $('.drawer__toggle > span.fa').addClass('fa-angle-left');
    }
}

function toggleLedgend() {
    $('#legend').toggle('.hide');

    if (!$('#legend-switch').checked) {
        $('#legend').addClass('hide');
    }
}

function stickyToolbar(event) {
    var scroll = $(window).scrollTop();
    if ( scroll > 140 ) {
        $('.toolbar').addClass("fixed");
    } else {
        $('.toolbar').removeClass("fixed");
    }
}

function toggleImportant() {
    $okRow = $('.status-column:contains("OK")').parent();
    if ($('#show-important').prop("checked")) {
        $okRow.css("display", "none");
    } else {
        $okRow.css("display", "table-row");
    }
}

function multiSelect(e) {
    if(!$lastChecked) {
        $lastChecked = this;
        return;
    }
    if(e.shiftKey) {
        var start = $checkbox.index(this);
        var end = $checkbox.index($lastChecked);
        $checkbox.slice(Math.min(start,end), Math.max(start,end) + 1).prop('checked', $lastChecked.checked);
    }
    lastChecked = this;
}


function documentReady(){
    $checkbox = $('.select-column input[type="checkbox"]');
    $okRow = $('.status-column:contains("OK")').parent();

    $(window).scroll(stickyToolbar);

    toggleImportant();

    $('input[name="group1[]"]').on("click", toggleImportant);

    $checkbox.on("click", multiSelect);

    if ($('body').hasClass('page-agency')) {
        $(".nav li:nth-child(1)").removeClass("nav__item").addClass("nav__item--current");
    } else if ($('body').hasClass('page-dashboard')) {
        $(".nav li:nth-child(2)").removeClass("nav__item").addClass("nav__item--current");
    } else if ($('body').hasClass('page-notes')) {
        $(".nav li:nth-child(3)").removeClass("nav__item").addClass("nav__item--current");
    }
}

$('body').on("click", ".drawer__toggle", toggleDrawer);
$("body").on("click", ".ackButton", erts.setEvent);

$('body').on("click", "#legend-switch", toggleLedgend);

$('thead th').attr('scope', 'col');
$('#clearButton').data("note_type", 2);
$('#addButton').data("note_type", 1);



$(document).ready(documentReady);